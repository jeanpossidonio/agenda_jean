-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09-Abr-2015 às 00:56
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agenda_jean`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
`id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `endereco` varchar(200) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `complemento` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `nome`, `telefone`, `celular`, `email`, `endereco`, `numero`, `bairro`, `cidade`, `complemento`) VALUES
(1, 'Jean Carlos PossidÃ´nio da Silva', '14 998525177', '14 998525177', 'jc.possidonio@gmail.com', 'Rua Jussara Maria', '830', 'Centro', 'Avare', 'Perto do Bar do Jorge'),
(6, 'Manoel da Silva', '14 658963256', '14658966325', 'manoel@silva.com.br', 'Rua Manoel Silva', '258', 'Portugal', 'Bauru', 'Centro'),
(7, 'Jose Manoel da Silva', '69 63256985', '14 658966325', 'jose_email@webmail.com.br', 'Rua GetÃºlio Vargas', '1569', 'Centro', 'Cidade', 'Complemento'),
(8, 'Megan Fox', '14 669856325', '14 998569856', 'megan_fox@transformers.com.br', 'Rua Bubble Bee', '171', 'Centro', 'Bauru', 'Complemento'),
(9, 'JoÃ£o de Oliveira', '14 998569658', '14 665899658', 'joaooliveira@email.com.br', 'Rua das Oliveiras', '156', 'Jardim Floresta', 'Bauru', 'Complemento'),
(10, 'Aline Fernanda da Silva', '14 665896523', '14 665896523', 'aline@fernanda.com.br', 'Avenida do Sol', '145', 'Centro', 'MarÃ­lia', 'Centro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `password`, `status`) VALUES
(4, 1, 'jeancarlos', '8afdf992a468104642e2911e75dbbdd437f66c68', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contatos`
--
ALTER TABLE `contatos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contatos`
--
ALTER TABLE `contatos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
