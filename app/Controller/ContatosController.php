<?php
    class ContatosController extends AppController { 
        public $helpers = array("Form", "Html"); 

        public function index() { 
            $this->set("title", "Usuários"); 
            $contatos = $this->Contato->find('all'); 
            $this->set('contatos', $contatos); 
        }                                 

        public function adicionar() { 
            $this->set('title', 'Adicionar contato'); 
            if ($this->request->is("post")) { 
                $this->Contato->create(); 
                if ($this->Contato->saveAssociated($this->request->data)) { 
                    $this->Session->setFlash(__("Registro salvo com sucesso.")); 
                    $this->redirect(array("action" => '/index/')); 
                } else { 
                    $this->Session->setFlash(__("Erro: não foi possível salvar o registro.")); 
                    $this->redirect(array("action" => '/adicionar/')); 
                } 
            } 
        }

        public function editar($id = NULL) { 
            $this->set("title", "Editar Contato"); 
            $this->Contato->id = $id; 
            if (!$this->Contato->exists()) { 
                throw new NotFoundException(__('Registro não encontrado.')); 
            } 
            if ($this->request->is('post') || $this->request->is('put')) { 
                if ($this->Contato->saveAssociated($this->request->data)) { 
                    $this->Session->setFlash(__('Registro salvo com sucesso.')); 
                    $this->redirect(array('action' => '/index/')); 
                } else { 
                    $this->Session->setFlash(__('Erro: não foi possível salvar o registro.')); 
                } 
            } else { 
                $this->request->data = $this->Contato->read(NULL, $id); 
            } 
        }                                                                                                   

        public function excluir($id = NULL) { 
            if (!$this->request->is('get')) { 
                throw new MethodNotAllowedException(); 
            } 
            $this->Contato->id = $id; 
            if (!$this->Contato->exists()) { 
                throw new NotFoundException(__('Registro não encontrado.')); 
            } 
            if ($this->Contato->delete()) { 
                $this->Session->setFlash(__('Registro excluído com sucesso.')); 
                $this->redirect(array('action' => '/index/')); 
            } 
            $this->Session->setFlash(__('Erro: não foi possível excluir o registro.')); 
            $this->redirect(array('action' => '/index/')); 
        }

    }
?>
